package com.kostmo.stash.hooks.forcepush;

import java.io.IOException;
import java.util.Collection;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;

import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.revwalk.RevWalk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.hook.HookResponse;
import com.atlassian.stash.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.kostmo.stash.hooks.forcepush.RefValidator.FieldError;

public class ProtectRefHook implements PreReceiveRepositoryHook {

	public static String HOOK_NAME = "Force Push Branch Protector";

	private static final Logger LOG = LoggerFactory.getLogger(ProtectRefHook.class);
	private final HistoryService historyService;
	private final ApplicationPropertiesService applicationPropertiesService;

	public ProtectRefHook(HistoryService historyService, ApplicationPropertiesService applicationPropertiesService) {
		LOG.info("Initializing {}", ProtectRefHook.class.getName());
		this.historyService = historyService;
		this.applicationPropertiesService = applicationPropertiesService;
	}

	boolean refMatchesRegexes(RefChange refChange, Collection<String> regex_strings) {

		for (String regex_string : regex_strings)
			if (isRefCoveredByHookSettings(regex_string, refChange.getRefId()))
				return true;

		return false;
	}


	/**
	 * Disable force pushes to a ref
	 */
	@Override
	public boolean onReceive(@Nonnull RepositoryHookContext repositoryHookContext, @Nonnull Collection<RefChange> refChanges, @Nonnull HookResponse hookResponse) {


		org.eclipse.jgit.lib.Repository jgit_repository = null;

		try {

			jgit_repository = StashUtils.getJGitRepoFromStashRepo(applicationPropertiesService, repositoryHookContext.getRepository());
			RefValidator.validateConfig(jgit_repository, repositoryHookContext.getSettings()).throwFirstException();

		} catch (Exception e) {
			hookResponse.err().println( String.format("Error in %s hook configuration:\n%s", HOOK_NAME, e.getMessage()));
			e.printStackTrace();
			return false;
		} finally {
		    if (jgit_repository != null) {
		        jgit_repository.close();
		    }
		}


		try {

			Collection<String> regex_strings = RefValidator.getRefnameRegexStrings(jgit_repository, repositoryHookContext.getSettings());

			for (RefChange refChange : refChanges) {
				LOG.debug("Checking ref:{}, type:{}, from:{}, to:{}", new Object[]{refChange.getRefId(), refChange.getType(), refChange.getFromHash(), refChange.getToHash()});

				if (refMatchesRegexes(refChange, regex_strings) && isForcePush(jgit_repository, refChange)) {
					String errorMessage = String.format("Forced pushes to \"%s\" are not allowed by %s hook configuration.", refChange.getRefId(), HOOK_NAME);
					LOG.warn(errorMessage);
					hookResponse.err().println(errorMessage);
					return false;
				}
			}
			
		} catch (IOException e) {
			hookResponse.err().println(e.getMessage());
			e.printStackTrace();
			return false;
		} catch (FieldError e) {
			hookResponse.err().println(e.getMessage());
			e.printStackTrace();
			return false;
		} finally {
            if (jgit_repository != null) {
                jgit_repository.close();
            }
        }


		return true;
	}

	/**
	 * Checks if the given ref change is a forced push by checking whether
	 * the previous tip of the ref is an ancestor of the new tip of the ref
	 *
	 * @param repository
	 * @param refChange
	 * @return
	 * @throws IOException 
	 */
	boolean isForcePush(org.eclipse.jgit.lib.Repository jgit_repository, RefChange refChange) throws IOException {

		RevWalk walk = new RevWalk(jgit_repository);
		ObjectId previous_server_branch_commit = jgit_repository.resolve(refChange.getFromHash());
		ObjectId newer_local_branch_commit = jgit_repository.resolve(refChange.getToHash());
		boolean is_built_atop_ancestor = walk.isMergedInto(walk.parseCommit(previous_server_branch_commit), walk.parseCommit(newer_local_branch_commit));
		walk.release();

		return !is_built_atop_ancestor;
	}

	/**
	 * @param repositoryHookContext
	 * @param refName
	 * @return
	 */
	boolean isRefCoveredByHookSettings(String refIdsRegexString, String refName) {
		if(refIdsRegexString != null && !"".equals(refIdsRegexString)) {
			Pattern refIdsRegexPattern = Pattern.compile(refIdsRegexString);
			return refIdsRegexPattern.matcher(refName).matches();
		} else {
			return true;
		}
	}
}
