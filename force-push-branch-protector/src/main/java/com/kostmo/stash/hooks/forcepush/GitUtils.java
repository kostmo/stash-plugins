package com.kostmo.stash.hooks.forcepush;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.jgit.errors.CorruptObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.FileMode;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;

public class GitUtils {

	// ========================================================================
	public static Collection<File> listRepoFiles(
			File git_repo_directory,
			String gitref, File path,
			boolean recursive) throws IOException {

		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		Repository repository = builder.setGitDir(git_repo_directory).build();

		Collection<File> file_list = listPaths(repository, gitref, path, recursive);
		return file_list;
	}

	// ========================================================================
	public static InputStream getPathInputStreamAtRevision(
			Repository repository,
			ObjectId lastCommitId,
			File file_path) throws MissingObjectException, IOException {

		// now we have to get the commit
		RevWalk revWalk = new RevWalk(repository);
		RevCommit commit = revWalk.parseCommit(lastCommitId);
		// and using commit's tree find the path
		RevTree tree = commit.getTree();
		TreeWalk treeWalk = new TreeWalk(repository);
		treeWalk.addTree(tree);
		treeWalk.setRecursive(true);	// XXX This must be true
		treeWalk.setFilter(PathFilter.create(file_path.getPath()));
		InputStream returnStream;
        if (!treeWalk.next()) {
            returnStream = null;
        } else {
            ObjectId objectId = treeWalk.getObjectId(0);
            ObjectLoader loader = repository.open(objectId);
            returnStream = loader.openStream();
        }

        revWalk.release();
        treeWalk.release();
        return returnStream;
	}

	// ========================================================================
	
	/**
	 * Returns a list of immediate subdirectories when recurse_directories is false.
	 */
	public static Collection<File> listPaths(
			Repository repository,
			String gitref,
			File parent_path,
			boolean recurse_directories) throws MissingObjectException, IncorrectObjectTypeException, CorruptObjectException, IOException {

		ObjectId lastCommitId = repository.resolve(gitref);
		// now we have to get the commit
		RevWalk revWalk = new RevWalk(repository);
		RevCommit commit = revWalk.parseCommit(lastCommitId);
		// and using commit's tree find the path
		RevTree tree = commit.getTree();
		TreeWalk treeWalk = new TreeWalk(repository);
		treeWalk.addTree(tree);
		treeWalk.setFilter(PathFilter.create(parent_path.getPath()));
		
		treeWalk.setRecursive(recurse_directories);

		Collection<File> paths = new ArrayList<File>();
		
		if (recurse_directories) {
			while (treeWalk.next())
				paths.add(new File(treeWalk.getPathString()));
			
		} else {
			// Only fetch immediate children
			
			while (treeWalk.next()) {
				if (treeWalk.isSubtree() && treeWalk.getDepth() < 1)
					treeWalk.enterSubtree();

				// Return only directories
				if (treeWalk.getFileMode(0) == FileMode.TREE)
					paths.add(new File(treeWalk.getPathString()));
			}
		}
		
		revWalk.release();
        treeWalk.release();
		
		return paths;
	}
}
