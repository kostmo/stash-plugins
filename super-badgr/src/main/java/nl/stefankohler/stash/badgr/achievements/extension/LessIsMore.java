package nl.stefankohler.stash.badgr.achievements.extension;

import nl.stefankohler.stash.badgr.Achievement;

/**
 * The LessIsMore Achievement.
 * Granted if you checked in over 50 less files to Stash.
 * 
 * @author Stefan Kohler
 * @since 1.0
 */
@Achievement
public class LessIsMore extends ExtensionBasedAchievement {

    private static final Integer COUNTING_LIMIT = 50;

    @Override
    public String getBadge() {
        return "lessismore.png";
    }

    @Override
    public String getExtension() {
        return "less";
    }

    @Override
    public Integer getCountingLimit() {
        return COUNTING_LIMIT;
    }

}
