package com.teslamotors.bitbucket.plugins.rest;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.ByteStreams;

/**
 * To query this REST API, use this curl command:
 * curl http://localhost:7990/bitbucket/rest/command-line-runner/latest/api --user admin:admin
 * 
 * @author kostmo
 *
 */
@Path("/api")
@Produces({MediaType.APPLICATION_JSON})
public class CommandRunnerRestResource {

    private static final Logger log = LoggerFactory.getLogger(CommandRunnerRestResource.class);

	private final ApplicationPropertiesService application_properties_service;
	private final RepositoryService repo_service;

	CommandRunnerRestResource(ApplicationPropertiesService applicationPropertiesService, RepositoryService repo_service) {
		this.application_properties_service = applicationPropertiesService;
		this.repo_service = repo_service;
	}

	public static InputStream cmdExec(String cmdLine) throws IOException {
		Process p = Runtime.getRuntime().exec(cmdLine);
		
		return p.getInputStream();
	}
	
	static class InterdiffEncapsulator {
		
		final String old_base, old_tip, new_base, new_tip;
		final File repo_dir;
		public InterdiffEncapsulator(File repo_dir, String old_base, String old_tip, String new_base, String new_tip) {
			
			this.repo_dir = repo_dir;
			
			this.old_base = old_base;
			this.old_tip = old_tip;
			this.new_base = new_base;
			this.new_tip = new_tip;
		}
		
		public String makeCommandString() {
			return String.format("relative_diff.sh %s %s %s %s %s",
					this.repo_dir.getAbsolutePath(),
					this.old_base,
					this.old_tip,
					this.new_base,
					this.new_tip);
		}
	}
	
	@GET
	public Response runInterdiff(
			@QueryParam("project_key") final String project_key,
			@QueryParam("repo_slug") final String repo_slug,
			@QueryParam("old_base") final String old_base,
			@QueryParam("old_tip") final String old_tip,
			@QueryParam("new_base") final String new_base,
			@QueryParam("new_tip") final String new_tip) throws IOException {

		Repository repository = this.repo_service.getBySlug(project_key, repo_slug);
		File repo_dir = application_properties_service.getRepositoryDir(repository);

		InterdiffEncapsulator range_encapsulator = new InterdiffEncapsulator(repo_dir, old_base, old_tip, new_base, new_tip);
		
		try {
			String command_string = range_encapsulator.makeCommandString();
			
			System.out.println("##### COMMAND STRING: " + command_string);
			
			final InputStream commandOutput = cmdExec(command_string);

			// See link for more info on StreamingOutput
			// http://java.dzone.com/articles/jax-rs-streaming-response
			StreamingOutput streamingOutput = new StreamingOutput() {
			    @Override
			    public void write(OutputStream os) throws IOException {
			        ByteStreams.copy(commandOutput, os);
			        commandOutput.close();
			        os.flush();
			    }
			};

			return Response.ok(streamingOutput).build();
			
		} catch (IOException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}
}
