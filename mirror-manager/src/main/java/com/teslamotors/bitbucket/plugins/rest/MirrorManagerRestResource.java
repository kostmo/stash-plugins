package com.teslamotors.bitbucket.plugins.rest;

import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.rest.util.ResourcePatterns;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sun.jersey.spi.resource.Singleton;
import com.teslamotors.bitbucket.plugins.AsyncPostReceivePusher;
import com.teslamotors.bitbucket.plugins.settings.MirrorSettings;
import com.teslamotors.bitbucket.plugins.settings.MirrorSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

@Consumes({MediaType.APPLICATION_JSON})
@Path(ResourcePatterns.REPOSITORY_URI)
@Singleton
public class MirrorManagerRestResource {

    private final MirrorSettingsService mirrorSettingsService;
    private final PermissionValidationService permissionValidationService;
    private final ApplicationPropertiesService applicationPropertiesService;

    private final Logger logger = LoggerFactory.getLogger("com.atlassian.bitbucket");

    MirrorManagerRestResource(MirrorSettingsService mirrorSettingsService,
                              PermissionValidationService permissionValidationService,
                              ApplicationPropertiesService applicationPropertiesService) {
		this.mirrorSettingsService = mirrorSettingsService;
		this.permissionValidationService = permissionValidationService;
        this.applicationPropertiesService = applicationPropertiesService;
	}

    @GET
    @Path("/settings")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMirrors(@Context Repository repository) {
        MirrorSettings[] mirrorSettings = mirrorSettingsService.getAllMirrors(repository);

        List<MirrorJsonModel> mirrorModels = Lists.newArrayList();
        for (MirrorSettings settings : mirrorSettings) {
            mirrorModels.add(new MirrorJsonModel(settings.getID(), settings.getName(), settings.getUrl(), settings.getEnabled()));
        }

        return Response.ok(mirrorModels).build();
    }

    @PUT
    @Path("/settings")
    public Response addMirror(@Context Repository repository, MirrorJsonModel mirror) {
        permissionValidationService.validateForRepository(repository, Permission.REPO_ADMIN);

        mirrorSettingsService.addMirror(repository, mirror.name, mirror.url);

        return Response.ok().build();
    }

    @POST
    @Path("/settings")
    public Response updateMirror(@Context Repository repository, MirrorJsonModel mirror) {
        permissionValidationService.validateForRepository(repository, Permission.REPO_ADMIN);

        mirrorSettingsService.updateMirror(repository, mirror.id, mirror.name, mirror.url, mirror.enabled);

        return Response.ok().build();
    }

    @DELETE
    @Path("/settings")
    public Response deleteMirror(@Context Repository repository, MirrorJsonModel mirror) {
        permissionValidationService.validateForRepository(repository, Permission.REPO_ADMIN);

        mirrorSettingsService.deleteMirror(repository, mirror.id);

        return Response.ok().build();
    }

    @PUT
    @Path("/manualPush")
    public Response manualPush(@Context Repository repository, MirrorJsonModel mirror) {
        permissionValidationService.validateForRepository(repository, Permission.REPO_ADMIN);

        MirrorSettings mirrorSettings = mirrorSettingsService.getMirrorById(repository, mirror.id);

        if (mirrorSettings != null) {
            Map<String, Semaphore> pushesInProgress = Maps.newConcurrentMap();
            Collection<String> remote = Lists.newArrayList(mirrorSettings.getUrl());

            AsyncPostReceivePusher.pushToRemotes(repository, remote, pushesInProgress, logger, applicationPropertiesService);
        }

        return Response.ok().build();
    }
}
