package com.teslamotors.bitbucket.plugins;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.bitbucket.event.repository.RepositoryDeletedEvent;
import com.atlassian.event.api.EventListener;
import com.teslamotors.bitbucket.plugins.settings.MirrorSettingsService;

@SuppressWarnings("unused")
public class RepositoryDeletedEventListener {

    private final MirrorSettingsService mirrorSettingsService;

    public RepositoryDeletedEventListener(ActiveObjects ao) {
        this.mirrorSettingsService = new MirrorSettingsService(ao);
    }

    @EventListener
    public void deleteRepoMirrorSettings(RepositoryDeletedEvent event) {
        mirrorSettingsService.deleteMirrorsForRepo(event.getRepository());
    }
}
