package com.teslamotors.bitbucket.plugins.interdiff;

import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * This servlet handles /interdiffer/projects/${P_KEY}/repos/${r_slug}/pull-request/${pr_id}
 * It only creates facade for REST handle. The content of tab served by REST API.
 *
 * @author Alexey Efimov
 */
public class InterdiffServlet extends HttpServlet {
    private static final String SOY_MODULE = "com.atlassian.bitbucket.server.bitbucket-web:server-soy-templates";
    private static final String VIEW = "bitbucket.internal.page.pullRequest.v2.view";
    private static final String CONFLICTS_TAB_KEY = "bitbucket.pull-request.nav.interdiff-item";
    private static final String PROJECTS_KEY = "projects";
    private static final String REPOS_KEY = "repos";
    private static final String PULL_REQUESTS_KEY = "pull-requests";

    private final RepositoryService repositoryService;
    private final PermissionService permissionService;
    private final PullRequestService pullRequestService;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final AuthenticationContext authenticationContext;
    private final ApplicationPropertiesService propertiesService;

    public InterdiffServlet(RepositoryService repositoryService,
                            PermissionService permissionService,
                            PullRequestService pullRequestService,
                            SoyTemplateRenderer soyTemplateRenderer,
                            AuthenticationContext authenticationContext,
                            ApplicationPropertiesService propertiesService) {
        this.repositoryService = repositoryService;
        this.permissionService = permissionService;
        this.pullRequestService = pullRequestService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.authenticationContext = authenticationContext;
        this.propertiesService = propertiesService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> params = requestToMap(req, PROJECTS_KEY, REPOS_KEY, PULL_REQUESTS_KEY);
        if (params.size() == 3) {
            Repository repo = repositoryService.getBySlug(
                    params.get(PROJECTS_KEY), params.get(REPOS_KEY));
            if (repo != null) {
                PullRequest pr = pullRequestService.getById(repo.getId(),
                        Long.parseLong(params.get(PULL_REQUESTS_KEY)));
                if (pr != null) {
                    // This soy template is _not_ considered part of Bitbucket core API,
                    // and as such is subject to change at any point.
                    render(resp, SOY_MODULE, VIEW, contextParams(pr, repo));
                } else {
                    resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Pull Request not found");
                }
            } else {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Repository not found");
            }
        } else {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    /**
     * Render Soy template with params
     *
     * @param resp     HTTP Servlet response
     * @param module   Soy Module to render template
     * @param template Template to render
     * @param params   Params for template render
     * @throws IOException
     * @throws ServletException
     */
    private void render(HttpServletResponse resp, String module, String template, Map<String, Object> params)
            throws IOException, ServletException {
        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(), module, template, params, globalContextParams());
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }

    /**
     * Context params for Soy to render Pull Request Conflicts tab.
     *
     * @param pullRequest Pull Request
     * @return Context params map
     */
    private Map<String, Object> contextParams(PullRequest pullRequest, Repository repository) {
        ImmutableMap<String, ImmutableMap<String, Boolean>> permissions = ImmutableMap.of("repository",
                ImmutableMap.of("write", permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE)));

        return ImmutableMap.<String, Object>builder()
                .put("repository", repository)
                .put("pullRequest", pullRequest)
                .put("permissions", permissions)
                .put("startingTabKey", CONFLICTS_TAB_KEY)
                        // TODO: These two values are wrong and shouldn't be hard-coded, but
                        // TODO: can't easily be loaded by a plugin
                .put("maxChanges", 1000)
                .put("seenNeedsWork", true)
                .build();
    }

    /**
     * TODO This is currently only available to Spring MVC controllers, which sadly isn't possible in plugins.
     * <p/>
     * See {@code DefaultInjectedDataFactory.java} for all the missing arguments, these are just the bare minimum.
     *
     * @return Global context params map
     */
    private Map<String, Object> globalContextParams() {
        return ImmutableMap.<String, Object>builder()
                .put("instanceName", propertiesService.getDisplayName())
                .put("principal", authenticationContext.getCurrentUser())
                        // We don't _need_ this, but it should be there
                .put("timezone", -(propertiesService.getDefaultTimeZone().getOffset(System.currentTimeMillis()) >> 4))
                        // Avoid problem with '$ij.language'
                .put("language", "en_US")
                .build();
    }

    /**
     * Extract path-info to map by keys specified
     *
     * @param req  Servlet request
     * @param keys Keys to find in path-info
     * @return Map of found keys and values
     */
    private Map<String, String> requestToMap(HttpServletRequest req, String... keys) {
        ImmutableMap.Builder<String, String> params = ImmutableMap.builder();

        String pathInfo = req.getPathInfo();
        if (pathInfo != null) {
            pathInfo = pathInfo.startsWith("/") ? pathInfo.substring(1) : pathInfo;
            List<String> pathParts = Arrays.asList(pathInfo.split("/"));
            for (String key : keys) {
                int i = pathParts.indexOf(key);
                if (i != -1 && i < pathParts.size() - 1) {
                    params.put(key, pathParts.get(i + 1));
                }
            }
        }

        return params.build();
    }
}