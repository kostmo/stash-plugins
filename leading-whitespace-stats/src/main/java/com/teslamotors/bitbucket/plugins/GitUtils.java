package com.teslamotors.bitbucket.plugins;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.jgit.lib.FileMode;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;

public class GitUtils {

	// ========================================================================
	public static String fixWindowsPath(File file) {
		return file.getPath().replace("\\", "/");
	}
	
	// ========================================================================
	public static Collection<File> listRepoFiles(
			File git_repo_directory,
			String gitref, File path,
			boolean recursive) throws IOException {

		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		try (Repository repository = builder.setGitDir(git_repo_directory).build()) {
			return listPaths(repository, gitref, path, recursive);
		}
	}

	// ========================================================================
	public static InputStream getPathInputStreamAtRevision(
			Repository repository,
			String gitref,
			File file_path) throws IOException {

		try (RevWalk revWalk = new RevWalk(repository); TreeWalk treeWalk = new TreeWalk(repository)) {
			ObjectId lastCommitId = repository.resolve(gitref);
			// now we have to get the commit
			RevCommit commit = revWalk.parseCommit(lastCommitId);
			// and using commit's tree find the path
			RevTree tree = commit.getTree();
			treeWalk.addTree(tree);
			treeWalk.setRecursive(true);    // XXX This must be true
			treeWalk.setFilter(PathFilter.create(fixWindowsPath(file_path)));
			InputStream returnStream;
			if (!treeWalk.next()) {
				returnStream = null;
			} else {
				ObjectId objectId = treeWalk.getObjectId(0);
				ObjectLoader loader = repository.open(objectId);
				returnStream = loader.openStream();
			}

			return returnStream;
		}
	}

	// ========================================================================
	
	/**
	 * Returns a list of immediate subdirectories when recurse_directories is false.
	 */
	public static Collection<File> listPaths(
			Repository repository,
			String gitref,
			File parent_path,
			boolean recurse_directories) throws IOException {

		ObjectId lastCommitId = repository.resolve(gitref);
		// now we have to get the commit
		try (RevWalk revWalk = new RevWalk(repository); TreeWalk treeWalk = new TreeWalk(repository)) {
			RevCommit commit = revWalk.parseCommit(lastCommitId);
			// and using commit's tree find the path
			RevTree tree = commit.getTree();
			treeWalk.addTree(tree);
			treeWalk.setFilter(PathFilter.create(fixWindowsPath(parent_path)));

			treeWalk.setRecursive(recurse_directories);

			Collection<File> paths = new ArrayList<>();

			if (recurse_directories) {
				while (treeWalk.next())
					paths.add(new File(treeWalk.getPathString()));

			} else {
				// Only fetch immediate children
				while (treeWalk.next()) {
					if (treeWalk.isSubtree() && treeWalk.getDepth() < 1)
						treeWalk.enterSubtree();

					// Return only directories
					if (treeWalk.getFileMode(0) == FileMode.TREE)
						paths.add(new File(treeWalk.getPathString()));
				}
			}

			return paths;
		}
	}
}
