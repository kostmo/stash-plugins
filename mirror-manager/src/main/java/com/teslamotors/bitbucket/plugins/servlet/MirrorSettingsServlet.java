package com.teslamotors.bitbucket.plugins.servlet;

import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Map;

@SuppressWarnings("serial")
public class MirrorSettingsServlet extends HttpServlet {

    private static final String PLUGIN_KEY = "com.teslamotors.bitbucket.plugins.mirror-manager";

    private final RepositoryService repositoryService;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final PageBuilderService pageBuilderService;
    private final PermissionValidationService permissionValidationService;

    public MirrorSettingsServlet(RepositoryService repositoryService,
                                 SoyTemplateRenderer soyTemplateRenderer,
                                 PageBuilderService pageBuilderService,
                                 PermissionValidationService permissionValidationService) {
        this.repositoryService = repositoryService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.pageBuilderService = pageBuilderService;
        this.permissionValidationService = permissionValidationService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        if (Strings.isNullOrEmpty(pathInfo) || pathInfo.equals("/")) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        String[] pathParts = pathInfo.substring(1).split("/");
        if (pathParts.length != 2) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        String projectKey = pathParts[0];
        String repoSlug = pathParts[1];
        Repository repository = repositoryService.getBySlug(projectKey, repoSlug);
        if (repository == null) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        doView(repository, resp);
    }

    private void doView(Repository repository, HttpServletResponse resp) throws ServletException, IOException {
        permissionValidationService.validateForRepository(repository, Permission.REPO_ADMIN);
        render(resp,
                "bitbucket.page.mirrorOperations.repoSettingsPanel",
                ImmutableMap.<String, Object>builder()
                        .put("repository", repository)
                        .build()
        );
    }

    private void render(HttpServletResponse resp, String templateName, Map<String, Object> data) throws IOException, ServletException {
    	pageBuilderService.assembler().resources().requireContext("bitbucket.page.repository.settings.mirror-ops");
        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(), PLUGIN_KEY + ":mirror-ops-serverside-resources", templateName, data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }
}
