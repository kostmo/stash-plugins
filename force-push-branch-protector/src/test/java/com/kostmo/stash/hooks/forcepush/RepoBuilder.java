package com.kostmo.stash.hooks.forcepush;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;

/** Builds a test git repository specified by a type. */
public class RepoBuilder {
    
    /** Generates git repositories attempting to simulate two local repositories and a remote. 
     * @throws GitAPIException 
     * @throws IOException **/
    public static Repository getFakeRepo(String type) throws GitAPIException, IOException {
        
        File shared_repo_directory = new File("/tmp/ramdisk/shared_test_repo");
        if (shared_repo_directory.exists()) {
            recursivelyDelete(shared_repo_directory.getAbsolutePath());
            shared_repo_directory = new File("/tmp/ramdisk/shared_test_repo");
        }
        
        
        //hardcode authors for repo building
        PersonIdent archer = new PersonIdent(
                "Archer",
                "archer@example.com",
                new Date(0),
                TimeZone.getTimeZone("PST"));
        
        PersonIdent batman = new PersonIdent(
                "Batman",
                "batman@example.com",
                new Date(1),
                TimeZone.getTimeZone("PST"));
        
        switch(type) {
            //build a repo where batman can attempt to force push into a shared repo. 
            case "force_push":
                
                //Create repository
                Git git_commands_shared_repo = Git.init().setDirectory(shared_repo_directory).call();
                
                //create branches and test file, do initial commit.
                File test_file = new File("/tmp/ramdisk/shared_test_repo/test.txt");
                git_commands_shared_repo.add().addFilepattern("test.txt").call();
                git_commands_shared_repo.commit()
                    .setAuthor(archer)
                    .setCommitter(archer)
                    .setMessage("first commit").call();
                
                git_commands_shared_repo.branchCreate().setName("archer_branch").call();
                git_commands_shared_repo.branchCreate().setName("batman_branch").call();
                
                //Archer adds another file, commits to his local branch, and merges to master
                git_commands_shared_repo.checkout().setName("archer_branch").call();
                File archer_test_file = new File("/tmp/ramdisk/shared_test_repo/archer.txt");
                git_commands_shared_repo.add().addFilepattern("archer.txt").call();
                RevCommit commit = git_commands_shared_repo.commit()
                .setAuthor(archer)
                .setCommitter(archer)
                .setMessage("Archer's first commit").call();
                git_commands_shared_repo.checkout().setName("master").call();
                git_commands_shared_repo.merge().include(commit).call();
                
                //Batman commits to his local branch
                git_commands_shared_repo.checkout().setName("batman_branch").call();
                File batman_test_file = new File("/tmp/ramdisk/shared_test_repo/batman.txt");
                git_commands_shared_repo.add().addFilepattern("batman.txt").call();
                git_commands_shared_repo.commit()
                    .setAuthor(batman)
                    .setCommitter(batman)
                    .setMessage("batman's first commit").call();
                
                return git_commands_shared_repo.getRepository();         
                
        }
        return null;
    }
    
    /** Recursively deletes a given directory. */
    private static boolean recursivelyDelete(String directoryPath) {
        File file = new File(directoryPath);
        if (file.exists()) {
            //check if the file is a directory
            if (file.isDirectory()) {
                if ((file.list()).length > 0) {
                    for(String s:file.list()){
                        //call deletion of file individually
                        recursivelyDelete(directoryPath + File.separator + s);
                    }
                }
            }
            boolean result = file.delete();
            // test if delete of file is success or not
            if (!result) {
                System.out.println("File was not deleted, unknown reason");
            }
            return result;
        } else {
            System.out.println("File delete failed, file does not exists");
            return false;
        }
    }
}
