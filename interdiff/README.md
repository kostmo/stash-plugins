# Installation

First, install the `patchutils` Ubuntu package to obtain the `interdiff` program.

    sudo apt-get install patchutils python-pip
    sudo pip install simplediff

Next, copy the file `relative_diff.sh` from this directory to `/usr/bin/relative_diff.sh`.

Also copy the `diff2html.py` Python script (originally obtained from [here](http://git.droids-corp.org/gitweb/?p=diff2html.git) to `/usr/bin/diff2html.py`.

# Smoke Test Plan

1. Create a pull request and push changes to the target branch.
2. Verify the Incremental Review tab shows the diffs for each push.

* Push a new commit.
* Push a squashed commit.
* Rebase the target branch.