# Leading Whitespace Stats

Displays counts of indendation characters (Tabs vs. Spaces) as a "Client Web Item".

# Smoke Test Plan

Click on a source file and verify that the Indentation button shows what type of indentation the file has.

1. Push a file with space indentation only.
2. Push a file with tab indentation only.
3. Push a file with mixed indentation.