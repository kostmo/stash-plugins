package com.teslamotors.bitbucket.plugins.rest.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class LeadingWhitespaceCounter {

	// XXX
	static final boolean DEBUG = true;

	enum LeadingSpaceType {
		NONE, PURE_TABS, PURE_SPACES, MIXED
	}

	static boolean isIndentationChar(char c) {
		return c == IndentationCharacter.TAB.character || c == IndentationCharacter.SPACE.character;
	}

	public enum IndentationCharacter {

		SPACE(' '), TAB('\t');

		public char character;
		IndentationCharacter(char character) {
			this.character = character;
		}
	}

	IndentationCharacter getIndentationCharacter(char c) {
		for (IndentationCharacter ic : IndentationCharacter.values())
			if (ic.character == c)
				return ic;

		return null;
	}

	public static LeadingSpaceType processLine(String line, long line_number) {

		Character previous_indentation_char = null;
		
		for (int i=0; i<line.length(); i++) {
			final char c = line.charAt(i);
			if (isIndentationChar(c)) {

				if (previous_indentation_char != null && c != previous_indentation_char)
						return LeadingSpaceType.MIXED;

				previous_indentation_char = c;

			} else {
				break;
			}
		}

		if (Character.valueOf(IndentationCharacter.TAB.character).equals(previous_indentation_char))
			return LeadingSpaceType.PURE_TABS;
		else if (Character.valueOf(IndentationCharacter.SPACE.character).equals(previous_indentation_char))
			return LeadingSpaceType.PURE_SPACES;

		return LeadingSpaceType.NONE;
	}
	
	public static LeadingWhitespaceStatsModel check(InputStream is) throws IOException {

	    int pure_tabs_count = 0;
	    int pure_spaces_count = 0;
	    int mixed_indentation_count = 0;
	    
		BufferedReader br = new BufferedReader(new InputStreamReader(is));

		//Read File Line By Line
		String line = null;
		long line_index = 1;	// XXX Uses one-based indexing for readability
		while ((line = br.readLine()) != null)   {

			LeadingSpaceType leading_space_type = processLine(line, line_index);
			switch (leading_space_type) {
			case PURE_SPACES:
				pure_spaces_count++;
				break;
			case PURE_TABS:
				pure_tabs_count++;
				break;
			case MIXED:
				mixed_indentation_count++;
				break;
			default:
				break;
			}
			
			line_index++;
		}

		return new LeadingWhitespaceStatsModel(pure_tabs_count, pure_spaces_count, mixed_indentation_count);
	}
}