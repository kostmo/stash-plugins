package com.kostmo.stash.hooks.forcepush;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.annotation.Nonnull;

import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.setting.RepositorySettingsValidator;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.setting.SettingsValidationErrors;

public class RefValidator implements RepositorySettingsValidator {

	private static final Logger LOG = LoggerFactory.getLogger(RefValidator.class);

	private final ApplicationPropertiesService applicationPropertiesService;
	public RefValidator(ApplicationPropertiesService applicationPropertiesService) {
		this.applicationPropertiesService = applicationPropertiesService;
	}

	public static final String SETTINGS_FIELD_REGEX_BRANCH = "ref-ids-regex-branch";
	public static final String SETTINGS_FIELD_REGEX_PATH = "ref-ids-regex-path";

	static void validateRegexLine(String refIdsRegex) throws Exception {

		if (refIdsRegex == null || "".equals(refIdsRegex))
			throw new Exception("Regex cannot be null or empty.");

		Pattern.compile(refIdsRegex);
	}
	
	public static FieldAndFormErrors validateConfig(org.eclipse.jgit.lib.Repository jgit_repository, Settings settings) {
		FieldAndFormErrors errors = new FieldAndFormErrors();

		try {
			int line_number = 1;
			for (String regex_string : getRefnameRegexStrings(jgit_repository, settings)) {

				try {
					validateRegexLine(regex_string);
				} catch (PatternSyntaxException e) {
					String error_text = String.format("The regular expression syntax is invalid: %s", e.getMessage());
					errors.form_errors.add(new FormError( String.format("Problem on line %d of config file:\n%s", line_number, error_text) ) );
					
				} catch (Exception e) {
					errors.form_errors.add(new FormError( String.format("Problem on line %d of config file:\n%s", line_number, e.getMessage()) ) );
				}

				line_number++;
			}

		} catch (IOException e1) {
			errors.form_errors.add(new FormError(  e1.getMessage()) );
		} catch (FieldError e) {
			errors.field_errors.add(e);
		}
		
		return errors;
	}


	public static class FieldAndFormErrors {
		public Collection<FieldError> field_errors = new ArrayList<FieldError>();
		public Collection<FormError> form_errors = new ArrayList<FormError>();
		
		public void report(@Nonnull SettingsValidationErrors settingsValidationErrors) {
			for (FieldError x : field_errors)
				settingsValidationErrors.addFieldError(x.field, x.getMessage());
			
			for (FormError x : form_errors)
				settingsValidationErrors.addFormError(x.getMessage());
		}
		
		public void throwFirstException() throws Exception {
			
			Collection<Exception> exceptions = new ArrayList<Exception>();
			exceptions.addAll(field_errors);
			exceptions.addAll(form_errors);
			for (Exception e : exceptions)
				throw e;
		}
	}
	
	@SuppressWarnings("serial")
	public static class FieldError extends Exception {
		final public String field;
		FieldError(String field, String message) {
			super(message);
			this.field = field;
		}
	}
	
	@SuppressWarnings("serial")
	public static class FormError extends Exception {
		FormError(String message) {
			super(message);
		}
	}

	public static Collection<String> getRefnameRegexStrings(org.eclipse.jgit.lib.Repository jgit_repository, @Nonnull Settings settings) throws IOException, FieldError {

		InputStream is = StashUtils.getInputStreamFromSettings(
				jgit_repository, settings,
				SETTINGS_FIELD_REGEX_BRANCH, SETTINGS_FIELD_REGEX_PATH);

		BufferedReader in = new BufferedReader(new InputStreamReader(is));

		Collection<String> regex_strings = new ArrayList<String>();
		String line = null;
		while((line = in.readLine()) != null)
			regex_strings.add(line);

		in.close();

		return regex_strings;
	}

	@Override
	public void validate(@Nonnull Settings settings, @Nonnull SettingsValidationErrors settingsValidationErrors, @Nonnull Repository stash_repository) {

	    org.eclipse.jgit.lib.Repository jgit_repository = null;
		try {
			jgit_repository = StashUtils.getJGitRepoFromStashRepo(applicationPropertiesService, stash_repository);
			FieldAndFormErrors form_errors = validateConfig(jgit_repository, settings);
			form_errors.report(settingsValidationErrors);
			
		} catch (IOException e) {
			e.printStackTrace();
			settingsValidationErrors.addFormError(e.getMessage());
		} finally {
		    if (jgit_repository != null) {
		        jgit_repository.close();
		    }
		}
	}
}
