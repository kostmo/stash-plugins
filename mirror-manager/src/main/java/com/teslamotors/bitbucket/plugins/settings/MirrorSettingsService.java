package com.teslamotors.bitbucket.plugins.settings;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.bitbucket.repository.Repository;
import net.java.ao.Query;


public class MirrorSettingsService {

    private static final String MIRROR_BY_ID_QUERY = "PROJECT = ? and REPO = ? and ID = ?";
    private static final String MIRROR_BY_URL_QUERY = "PROJECT = ? and REPO = ? and URL = ?";
    private static final String ALL_MIRRORS_BY_REPO_QUERY = "PROJECT = ? and REPO = ?";
    private static final String ALL_ENABLED_MIRRORS_BY_REPO_QUERY = "PROJECT = ? and REPO = ? and ENABLED = ?";

    private final ActiveObjects ao;

    public MirrorSettingsService(ActiveObjects ao) {
        this.ao = ao;
    }

    private MirrorSettings[] findAll(String project, String repo) {
        return ao.find(MirrorSettings.class, Query.select().where(ALL_MIRRORS_BY_REPO_QUERY, project, repo));
    }

    private MirrorSettings[] findAllEnabled(String project, String repo) {
        return ao.find(MirrorSettings.class, Query.select().where(ALL_ENABLED_MIRRORS_BY_REPO_QUERY, project, repo, true));
    }

    private MirrorSettings[] findMirrorById(String project, String repo, Integer id) {
        return ao.find(MirrorSettings.class, Query.select().limit(1).where(MIRROR_BY_ID_QUERY, project, repo, id));
    }

    private MirrorSettings[] findMirrorByUrl(String project, String repo, String url) {
        return ao.find(MirrorSettings.class, Query.select().limit(1).where(MIRROR_BY_URL_QUERY, project, repo, url));
    }

    private MirrorSettings createMirrorSettings(String projectName, String repoName, String mirrorName, String mirrorUrl, Boolean enabled) {
        MirrorSettings newMirror = ao.create(MirrorSettings.class);
        newMirror.setProject(projectName);
        newMirror.setRepo(repoName);
        newMirror.setName(mirrorName);
        newMirror.setUrl(mirrorUrl);
        newMirror.setEnabled(enabled);
        newMirror.save();
        return newMirror;
    }

    /**
     * Returns mirrors configured for repository.
     */
    public MirrorSettings[] getAllMirrors(Repository repository) {
        return findAll(repository.getProject().getName(), repository.getName());
    }

    /**
     * Returns enabled mirrors configured for repository.
     */
    public MirrorSettings[] getEnabledMirrors(Repository repository) {
        return findAllEnabled(repository.getProject().getName(), repository.getName());
    }

    public MirrorSettings getMirrorById(Repository repository, Integer id) {
        MirrorSettings[] mirrors = findMirrorById(repository.getProject().getName(), repository.getName(), id);

        if (mirrors.length == 1) {
            return mirrors[0];
        } else {
            return null;
        }
    }

    /**
     * Only adds new mirror if mirrorUrl does not already exist. By default newly added mirrors are enabled.
     */
    public void addMirror(Repository repository, String mirrorName, String mirrorUrl) {
        ao.executeInTransaction(() -> {
            MirrorSettings[] mirrors = findMirrorByUrl(repository.getProject().getName(), repository.getName(), mirrorUrl);

            // Does a mirror with the url already exist?
            if (mirrors.length == 0) {
                createMirrorSettings(repository.getProject().getName(), repository.getName(), mirrorName, mirrorUrl, true);
            }

            return null;
        });
    }

    /**
     * Updates the mirror in the specified repository with matching id.
     */
    public void updateMirror(Repository repository, Integer id, String mirrorName, String mirrorUrl, Boolean enabled) {
        ao.executeInTransaction(() -> {
            MirrorSettings[] mirrorsById = findMirrorById(repository.getProject().getName(), repository.getName(), id);

            if (mirrorsById.length == 1) {
                MirrorSettings mirror = mirrorsById[0];
                mirror.setName(mirrorName);
                mirror.setUrl(mirrorUrl);
                mirror.setEnabled(enabled);
                mirror.save();
            }

            return null;
        });
    }

    /**
     * Deletes the mirror by name if it exists.
     */
    public void deleteMirror(Repository repository, Integer id) {
        ao.executeInTransaction(() -> {
            MirrorSettings[] mirrors = findMirrorById(repository.getProject().getName(), repository.getName(), id);

            if (mirrors.length == 1) {
                MirrorSettings mirror = mirrors[0];
                ao.delete(mirror);
            }

            return null;
        });
    }

    /**
     * Deletes all mirrors configured for the repository.
     */
    public void deleteMirrorsForRepo(Repository repository) {
        ao.executeInTransaction(() -> {
            MirrorSettings[] mirrors = findAll(repository.getProject().getName(), repository.getName());

            for (MirrorSettings mirror : mirrors) {
                ao.delete(mirror);
            }

            return null;
        });
    }
}
