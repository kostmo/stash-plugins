package com.kostmo.stash.hooks.forcepush;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static org.mockito.Mockito.*;

import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.hook.HookResponse;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.RefChangeType;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageRequestImpl;
import com.kostmo.stash.hooks.forcepush.ProtectRefHook;

@RunWith(MockitoJUnitRunner.class)
public class ProtectRefHookTest {

    @Mock
    private HistoryService historyService;

    @Mock
    private ApplicationPropertiesService applicationPropertiesService;
    
    private ProtectRefHook protectRefHook;

    @Before
    public void setup() {
        protectRefHook = new ProtectRefHook(historyService, applicationPropertiesService);
    }


    
//    @Test
//    public void testIsRefCoveredByHookSettings() {
//      
//      assertThat(true, is(true));
//    }
    
    @Test
    public void testIsRefCoveredByHookSettings() {
        Settings mockedSettings = mock(Settings.class);
        when(mockedSettings.getString(eq("ref-ids-regex"))).thenReturn("refs/heads/master");
        RepositoryHookContext mockedRepositoryHookContext = mock(RepositoryHookContext.class);
        when(mockedRepositoryHookContext.getSettings()).thenReturn(mockedSettings);
        
        String refIdsRegexString = mockedRepositoryHookContext.getSettings().getString("ref-ids-regex");
        assertThat(protectRefHook.isRefCoveredByHookSettings(refIdsRegexString, "refs/heads/master"), is(true));
        assertThat(protectRefHook.isRefCoveredByHookSettings(refIdsRegexString, "refs/heads/develop"), is(false));
    }

    @Test
    public void testIsRefCoveredByHookSettingsEmptySettings() {
        Settings mockedSettings = mock(Settings.class);
        when(mockedSettings.getString(eq("ref-ids-regex"))).thenReturn("");
        RepositoryHookContext mockedRepositoryHookContext = mock(RepositoryHookContext.class);
        when(mockedRepositoryHookContext.getSettings()).thenReturn(mockedSettings);

        String refIdsRegexString = mockedRepositoryHookContext.getSettings().getString("ref-ids-regex");
        assertThat(protectRefHook.isRefCoveredByHookSettings(refIdsRegexString, "refs/heads/master"), is(true));
        assertThat(protectRefHook.isRefCoveredByHookSettings(refIdsRegexString, "refs/heads/develop"), is(true));
    }

    @Test
    public void testIsRefCoveredByHookSettingsNullSettings() {
        Settings mockedSettings = mock(Settings.class);
        when(mockedSettings.getString(eq("ref-ids-regex"))).thenReturn(null);
        RepositoryHookContext mockedRepositoryHookContext = mock(RepositoryHookContext.class);
        when(mockedRepositoryHookContext.getSettings()).thenReturn(mockedSettings);

        String refIdsRegexString = mockedRepositoryHookContext.getSettings().getString("ref-ids-regex");
        assertThat(protectRefHook.isRefCoveredByHookSettings(refIdsRegexString, "refs/heads/master"), is(true));
        assertThat(protectRefHook.isRefCoveredByHookSettings(refIdsRegexString, "refs/heads/develop"), is(true));
    }

    @Test
    public void testIsForcePushIsForced() {
        Page mockedPage = mock(Page.class);
        when(mockedPage.getSize()).thenReturn(1);
        when(historyService.getChangesetsBetween(any(Repository.class), anyString(), anyString(), any(PageRequestImpl.class))).thenReturn(mockedPage);

        org.eclipse.jgit.lib.Repository testRepository = null;
        try {
            testRepository = RepoBuilder.getFakeRepo("force_push");

            RefChange mockedRefChange = mock(RefChange.class);
            when(mockedRefChange.getType()).thenReturn(RefChangeType.UPDATE);
            when(mockedRefChange.getFromHash()).thenReturn(testRepository.getRef("master").getObjectId().getName());
            when(mockedRefChange.getToHash()).thenReturn(testRepository.getRef("batman_branch").getObjectId().getName());

            assertThat(protectRefHook.isForcePush(testRepository, mockedRefChange), is(true));
        } catch (GitAPIException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } finally {
            if (testRepository != null) {
                testRepository.close();
            }
        }
    }
/*
    @Test
    public void testIsForcePushIsUpdate() {
        Page mockedPage = mock(Page.class);
        when(mockedPage.getSize()).thenReturn(0);
        when(historyService.getChangesetsBetween(any(Repository.class), anyString(), anyString(), any(PageRequestImpl.class))).thenReturn(mockedPage);

        Repository mockedRepository = mock(Repository.class);

        RefChange mockedRefChange = mock(RefChange.class);
        when(mockedRefChange.getType()).thenReturn(RefChangeType.UPDATE);
        when(mockedRefChange.getFromHash()).thenReturn("fromHash");
        when(mockedRefChange.getToHash()).thenReturn("toHash");

        assertThat(protectRefHook.isForcePush(mockedRepository, mockedRefChange), is(false));
    }

    @Test
    public void testIsForcePushIsDelete() {
        Page mockedPage = mock(Page.class);
        when(mockedPage.getSize()).thenReturn(1);
        when(historyService.getChangesetsBetween(any(Repository.class), anyString(), anyString(), any(PageRequestImpl.class))).thenReturn(mockedPage);

        Repository mockedRepository = mock(Repository.class);

        RefChange mockedRefChange = mock(RefChange.class);
        when(mockedRefChange.getType()).thenReturn(RefChangeType.DELETE);
        when(mockedRefChange.getFromHash()).thenReturn("fromHash");
        when(mockedRefChange.getToHash()).thenReturn("toHash");

        assertThat(protectRefHook.isForcePush(mockedRepository, mockedRefChange), is(false));
    }

    @Test
    public void testOnReceiveOk() {
        Settings mockedSettings = mock(Settings.class);
        when(mockedSettings.getString(eq("ref-ids-regex"))).thenReturn("refs/heads/master");
        RepositoryHookContext mockedRepositoryHookContext = mock(RepositoryHookContext.class);
        when(mockedRepositoryHookContext.getSettings()).thenReturn(mockedSettings);

        HookResponse mockedHookResponse = mock(HookResponse.class);

        Page mockedPage = mock(Page.class);
        when(mockedPage.getSize()).thenReturn(0);
        when(historyService.getChangesetsBetween(any(Repository.class), anyString(), anyString(), any(PageRequestImpl.class))).thenReturn(mockedPage);


        RefChange mockedRefChange = mock(RefChange.class);
        when(mockedRefChange.getType()).thenReturn(RefChangeType.UPDATE);
        when(mockedRefChange.getFromHash()).thenReturn("fromHash");
        when(mockedRefChange.getToHash()).thenReturn("toHash");
        when(mockedRefChange.getRefId()).thenReturn("refs/heads/master");

        List<RefChange> mockedRefChangeList = Arrays.asList(mockedRefChange);

        assertThat(protectRefHook.onReceive(mockedRepositoryHookContext, mockedRefChangeList, mockedHookResponse), is(true));


        verifyZeroInteractions(mockedHookResponse);
    }
    */
}
