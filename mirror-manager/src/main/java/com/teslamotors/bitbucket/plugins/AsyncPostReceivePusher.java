package com.teslamotors.bitbucket.plugins;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;


import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.google.common.base.Joiner;
import com.google.common.io.CharStreams;
import com.teslamotors.bitbucket.plugins.settings.MirrorSettings;
import com.teslamotors.bitbucket.plugins.settings.MirrorSettingsService;

import com.atlassian.bitbucket.hook.repository.AsyncPostReceiveRepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.CommandFailedException;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AsyncPostReceivePusher implements AsyncPostReceiveRepositoryHook {

    private final Logger logger = LoggerFactory.getLogger("com.atlassian.bitbucket");

    private final Map<String, Semaphore> pushes_in_progress = Maps.newConcurrentMap();

    private final MirrorSettingsService mirrorSettingsService;
	private final ApplicationPropertiesService applicationPropertiesService;

	AsyncPostReceivePusher(MirrorSettingsService mirrorSettingsService,
						   ApplicationPropertiesService applicationPropertiesService) {
        this.mirrorSettingsService = mirrorSettingsService;
		this.applicationPropertiesService = applicationPropertiesService;
	}

	static class Task implements Callable<String> {
		
		private final String url;
        private final Logger logger;
		private final Repository repository;
		private final Semaphore semaphore_for_repo;
		private final ApplicationPropertiesService applicationPropertiesService;
		
		public Task(String url, Repository repository, Semaphore semaphore_for_repo, Logger logger,
					ApplicationPropertiesService applicationPropertiesService) {
			this.url = url;
            this.logger = logger;
			this.repository = repository;
			this.semaphore_for_repo = semaphore_for_repo;
			this.applicationPropertiesService = applicationPropertiesService;
		}
		
	    @Override
	    public String call() {

			long start_time = System.currentTimeMillis();

			logger.debug(String.format("MIRROR PLUGIN: Now running command to push to url \"%s\"...", this.url));

			try {
				ProcessBuilder builder = new ProcessBuilder().command(Lists.newArrayList("/usr/bin/git", "push", "--mirror", this.url));
				builder.redirectErrorStream(true);
				builder.directory(this.applicationPropertiesService.getRepositoryDir(repository));

				Process p = builder.start();
				p.waitFor(5, TimeUnit.MINUTES);

				String mirror_push_command_output = CharStreams.toString(new InputStreamReader(p.getInputStream()));

				long end_time = System.currentTimeMillis();

				logger.debug(String.format(
						"MIRROR PLUGIN: Push to \"%s\" took %.1f seconds. Output from mirror push: %s",
						url,
						(end_time - start_time) / 1000.0,
						mirror_push_command_output));

				return mirror_push_command_output;
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
				return "Unable to complete push due to exception. See logs for stacktrace.";
			} finally {
				this.semaphore_for_repo.release();
				logger.debug(String.format("MIRROR PLUGIN: Released semaphore for push to \"%s\"", this.url));
			}
		}
	}

	private static Semaphore getOrCreateSemaphore(Map<String, Semaphore> pushes_in_progress, String url) {
		if (pushes_in_progress.containsKey(url)) {
			return pushes_in_progress.get(url);
		} else {
			Semaphore in_progress_semaphore = new Semaphore(1, true);
			pushes_in_progress.put(url, in_progress_semaphore);
			return in_progress_semaphore;
		}
	}
	
	/**
	 * Returns list of skipped remotes
	 */
	public static Collection<String> pushToRemotes(final Repository repository,
                                                   Collection<String> remotes, Map<String, Semaphore> pushes_in_progress, Logger logger,
												   ApplicationPropertiesService applicationPropertiesService) {

		Collection<String> skipped_remotes = Lists.newArrayList();
		ExecutorService pusher_executor_service = Executors.newFixedThreadPool(4);		
		Map<String, Future<String>> pushes_by_repo = Maps.newHashMap();

		// Non-blocking loop
		for (final String url : remotes) {

			Semaphore semaphore_for_repo = getOrCreateSemaphore(pushes_in_progress, url);
			Task task = new Task(url, repository, semaphore_for_repo, logger, applicationPropertiesService);

			// Don't start a new push for this repo if a previous push is still running
			if (semaphore_for_repo.tryAcquire()) {

				Future<String> future = pusher_executor_service.submit(task);
				pushes_by_repo.put(url, future);

			} else {
				logger.debug( String.format("MIRROR PLUGIN: Semaphore indicates push to \"%s\" already in progress", url) );
				skipped_remotes.add(url);
			}
		}


		for (Entry<String, Future<String>> entry : pushes_by_repo.entrySet()) {

			String repo_name = entry.getKey();
			Future<String> future = entry.getValue();
			
			try {
				
				String command_output = future.get(5, TimeUnit.MINUTES);

			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				e.printStackTrace();
				
				logger.debug( String.format("MIRROR PLUGIN: Caught exception of type \"%s\" while pushing to \"%s\"; Message: \"%s\"",
						e.getClass().getSimpleName(),
						repo_name,
						e.getMessage()));
				
			} catch (CommandFailedException e) {
			    logger.debug(String.format("MIRROR PLUGIN: Push Command Failed; Message: %s", e.getMessage()));
			}
		}
	
		// XXX We shouldn't need this...
		pusher_executor_service.shutdownNow();
		
		return skipped_remotes;
	}

	/**
	 * Cross-references the names of remotes that are enabled
	 * in the "Settings" object with remotes that are actually
	 * defined as mirrors in the Git config file.
	 * @throws IOException
	 */
	public final Collection<String> getEnabledRemotes(Repository repository)
			throws IOException {

        List<MirrorSettings> enabledMirrors = Lists.newArrayList(mirrorSettingsService.getEnabledMirrors(repository));

        return enabledMirrors.stream().map(mirrorSettings -> mirrorSettings.getUrl()).collect(Collectors.toList());
	}

	private String shortenHash(String hash) {
		return hash.substring(0, 7);
	}
	
	/**
	 * Pushes to the enabled mirror(s).
	 * 
	 * The pushToRemotes() function returns a list of remotes whose pushes
	 * were already in progress from previous refChanges, and were therefore
	 * skipped this time around.
	 * 
	 * If, say, multiple commits get pushed at the same time at the end of the
	 * day, commits could sit unpushed to the remotes over night. Therefore, maybe
	 * the members of the "skipped_remotes" list should be re-queued for
	 * pushing somehow. However, how long should we persist in our attempts to
	 * re-queue?  We need to return from the postReceive() function eventually...
	 */
	@Override
	public void postReceive(RepositoryHookContext context, Collection<RefChange> refChanges) {
		
		try {
			Collection<String> enabled_remotes = getEnabledRemotes(context.getRepository());
			
			Collection<String> branch_names = Lists.newArrayList();
			for (RefChange ref_change : refChanges) {
				branch_names.add(ref_change.getRef().getId() + " (" + shortenHash(ref_change.getFromHash()) + "->" + shortenHash(ref_change.getToHash()) + ")");
			}
			
			String push_set_indicator_string = "branches: " + branch_names + "; remotes: " + enabled_remotes;
			logger.debug( "MIRROR PLUGIN: Initiating pushes: " + push_set_indicator_string);

			long start_time = System.currentTimeMillis();
			Collection<String> skipped_remotes = pushToRemotes(context.getRepository(), enabled_remotes, pushes_in_progress, logger, applicationPropertiesService);
			long end_time = System.currentTimeMillis();
			
			logger.debug( String.format("MIRROR PLUGIN: Finished all pushes (skipping these: [%s]) in %.1f sec for: %s",
					Joiner.on(", ").join(skipped_remotes),
					(end_time - start_time)/1000.0,
					push_set_indicator_string) );
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}