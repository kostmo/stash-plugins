define('page/pull-request/view/pull-request-view-interdiff', [
    'jquery',
    'bitbucket/internal/model/page-state',
    'feature/pull-request/interdiffy-feature'
], function (
    $,
    pageState,
    pullRequestInterdiffyFeature
    ) {
    return {
        load : function(el) {
            return pullRequestInterdiffyFeature.init({
                el : el,
                pullRequest : pageState.getPullRequest()
            });
        },
        unload : function(el) {
        	pullRequestInterdiffyFeature.reset();
            $(el).empty();
        },
        keyboardShortcutContexts : ['pull-request-conflicts']
    };
});


// This is layout extension to 'layout/pull-request' for registration
// handler attached to 'pull-request-view'
define('layout/pull-request/interdiffy',
    /* parameters mapping */
    ['exports', 'bitbucket/internal/v2/layout/pull-request'],
    function (exports, pullRequestLayout) {
        // Registers the handler for a given feature, which in this case is a PR tab
        // This allows for purely client-side behaviour instead of having a full page refresh for each tab
        pullRequestLayout.registerHandler(
            // See atlassian-plugin.xml/web-item[@key='bitbucket.pull-request.nav.interdiff-item'] and also
            // ru.yandex.stash.plugin.pull.conflicts.PullRequestConflictsServlet#CONFLICTS_TAB_KEY
            'bitbucket.pull-request.nav.interdiff-item',
            // See atlassian-plugin.xml/servlet[@key='pull-request-conflicts-servlet']
            /^[^\?\#]*\/interdiffer\/.*?\/pull-requests\/\d+/,
            'page/pull-request/view/pull-request-view-interdiff');
    }
);

// Load explicit
require('layout/pull-request/interdiffy');
