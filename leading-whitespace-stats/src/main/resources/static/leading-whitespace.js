
define('plugin/leading-whitespace',
	['exports', 'jquery', 'lodash', 'aui'],
	function(exports, $, _, AJS) {

		function printObject(obj) {
			$.each(obj, function(key, element) {
				console.log('key: ' + key + '\n' + 'value: ' + element);
			});
		}

		var pageState = require('bitbucket/internal/model/page-state');
	
		function getProjectKey() {
			return pageState.getProject().getKey();
		}

		function getRepoSlug() {
			return pageState.getRepository().getSlug();
		}

		function getUrlBase() {
			return AJS.contextPath() + '/rest/leading-whitespace/latest';
		}

		exports.labelfunc = function(context) {

			var data;

			var succeeded = false;
			$.ajax({
				type: 'GET',
				url: getUrlBase() + '/api',
				dataType: 'json',
				success: function(data_result) {
					data = data_result;
					succeeded = true;
				},
				data: {
					project_key: getProjectKey(),
					repo_slug: getRepoSlug(),
					file_ref: context.fileChange.commitRange.untilRevision.id,
					file_path: context.fileChange.path.components.join("/")
				},
				async: false
			});
			
			if (!succeeded)
				return "<no file>";

			var total_counts = data.pure_tabs_count + data.pure_spaces_count + data.mixed_indentation_count;
			var tabs_percent = Math.floor(100.0*data.pure_tabs_count/total_counts);
			var spaces_percent = Math.floor(100.0*data.pure_spaces_count/total_counts);
			var mixed_percent = Math.floor(100.0*data.mixed_indentation_count/total_counts);

			var label_string;
			if (total_counts == 0) {
				label_string = "No indentation";
			} else if (mixed_percent == 0 && data.pure_spaces_count == 0) {
				label_string = "Indentation: All tabs";
			} else if (mixed_percent == 0 && data.pure_tabs_count == 0) {
				label_string = "Indentation: All spaces";
			} else {

				var sections = [];
				if (data.pure_tabs_count > 0)
					sections.push(data.pure_tabs_count + "/" + total_counts + " (" + tabs_percent + "%) tabs");

				if (data.pure_spaces_count > 0)
					sections.push(data.pure_spaces_count + "/" + total_counts + " (" + spaces_percent + "%) spaces");

				if (data.mixed_indentation_count > 0)
					sections.push(data.mixed_indentation_count + "/" + total_counts + " (" + mixed_percent + "%) mixed");

				label_string = "Indentation by line: " + sections.join(", ");
			}

			return label_string;
		}
	}
);
