# Repository mirror manager

Allows one to add new "remotes" to the Git config on per-repository basis, and enable those remotes as
mirrors that are pushed to as a post-receive hook.

## Mirror Setup

On the remote host, you need to initially create a mirror repo which will get updated automatically
by Bitbucket.

    cd /var/www/projects
    git clone --mirror <upstream repository URL>

Then you might need to change permissions:

    sudo chown -R gitsync:www-data <cloned repository>


## Hook Setup

You must do two things in the Bitbucket web interface:

1. Configure the mirror(s) in the "Mirror management" section of the repository settings page
1. Enable the hook in the "Hooks" section of the repository settings page
