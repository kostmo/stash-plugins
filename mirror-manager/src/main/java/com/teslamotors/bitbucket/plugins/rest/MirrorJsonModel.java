package com.teslamotors.bitbucket.plugins.rest;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class MirrorJsonModel {

    @JsonProperty
    public Integer id;

    @JsonProperty
    public String name;

    @JsonProperty
    public String url;

    @JsonProperty
    public Boolean enabled;

    /**
     * Used by Jackson object serialization/de-serialization.
     */
    public MirrorJsonModel() { }

    public MirrorJsonModel(Integer id, String name, String url, Boolean enabled) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.enabled = enabled;
    }
}
