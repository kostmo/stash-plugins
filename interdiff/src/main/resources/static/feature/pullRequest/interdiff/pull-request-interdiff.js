define('feature/pull-request/interdiffy-feature', [
    'jquery',
    'aui',
    'lodash',
    'bitbucket/internal/util/ajax',
    'bitbucket/internal/util/events',
    'bitbucket/internal/model/page-state',
    'exports'],
    function ($, AJS, _, ajax, events, pageState, exports) {

        function getProjectKey() {
			return pageState.getProject().getKey();
		}

		function getRepoSlug() {
			return pageState.getRepository().getSlug();
		}

		function getSystemCommandRestUrlBase() {
			return AJS.contextPath() + '/rest/command-line-runner/latest';
		}
        
        function getCommitsFrontendUrlBase() {
			return AJS.contextPath() + '/projects/' + getProjectKey() + '/repos/' + getRepoSlug() + '/commits/';
		}
        
        function getPullRequestUrlBase() {
			return AJS.contextPath() + '/rest/api/latest/projects/' + getProjectKey() + '/repos/' + getRepoSlug() + '/pull-requests/' + pageState.getPullRequest().id;
		}

        
        function isTruncatedList(activity_object, changeset_type) {
        	
        	var commit_holder = activity_object[changeset_type];
        	var commit_list = commit_holder.commits;

        	var commit_total = commit_holder.total;
        	var commit_list_length = commit_list.length;

        	var is_truncated = commit_list_length < commit_total;
        	if (is_truncated)
        		console.log("The commit total for Activity " + activity_object.id + " <" + changeset_type + "> is " + commit_total + "; the length is " + commit_list_length);

        	return is_truncated;
        }
        
        function getAncestorCommit(activity_object, changeset_type) {
        	
        	var commit_holder = activity_object[changeset_type];
        	var commit_list = commit_holder.commits;

        	var commit_total = commit_holder.total;
        	var ccommit_list_length = commit_list.length;
        	
        	// FIXME The changeset list in the JSON response is truncated at 5 items.
        	// How to retrieve the rest?
        	// See https://answers.atlassian.com/questions/219771/how-to-obtain-full-list-of-added-removed-commits-more-than-5-in-a-pull-request-activity-query

        	if (commit_total > 0) {
        		return commit_list[commit_total - 1];
        	} else {
        		return null;
        	}
        }
        

        function generateTOC(sorted_rescope_reports) {
        
        	var toc_div = $("<div>");
        	var overview = $("<p>");
        	overview.text("If any code modifications are made to this branch since the pull request was opened, they will be displayed here. These are generally changes made in response to reviewer feedback.");
        	toc_div.append(overview);
        	var toc_ul = $("<ul>");
        	toc_div.append(toc_ul);
        	toc_div.append($("<hr>"));

        	$.each(sorted_rescope_reports, function(index, rescope_report) {
            	var toc_li = $("<li>");
        		toc_ul.append(toc_li);
        		var toc_a = $("<a>", {"href": "#" + rescope_report.getAnchorName()});
        		toc_a.text(rescope_report.getTitle());
        		toc_li.append(toc_a);
        	});
        	
        	return toc_div;
        }
        
        function RescopeReport(activity_object) {
        	
	        this.activity_object = activity_object;
	        
	        this.diff_output = null;
	        this.success = false;
	        this.failure_reason = "Couldn't retrieve interdiff. Is the server set up properly?";
	        
	        this.getId = function() {
	        	return this.activity_object.id;
	        };
	        
	        this.getTitle = function() {
	        	return "Rescope at " + this.getFormattedDate();
	        };
	        
	        this.getAnchorName = function() {
	        	return "interdiff-anchor-" + this.getId();
	        };

	        this.getDate = function() {
	        	return new Date(this.activity_object.createdDate);
	        };
	        
	        this.getFormattedDate = function() {
	        	var d = this.getDate();
	        	return [d.getHours(), d.getMinutes(), d.getSeconds()].join(":") + ", " + [d.getMonth() + 1, d.getDate(), d.getFullYear()].join("/");
	        };
	        
	        this.getHtmlElement = function() {
	        	
	        	var div_container = $("<div/>");

        		var heading = $("<h2/>", {"id": this.getAnchorName()});
                heading.text(this.getTitle() + ":");
                div_container.append(heading);
	        	
	        	if (this.success) {

	                if (this.diff_output.length > 0) {

	                	var code_pre = $("<pre/>");
	                    var code_div = $("<code/>", {"class": "diff"});
	                    code_pre.append(code_div);
	                    code_div.html(this.diff_output);
	                    
	                    div_container.append(code_pre);

	                } else {
	                	
	                	var empty_message = $("<p>");
	                	empty_message.text("<no diff>");
	                	
	                    div_container.append(empty_message);
	                }

	        	} else {
	        		
                	var error_message = $("<p style='color: red'>");
                	error_message.text(this.failure_reason);

                	div_container.append(error_message);
	        	}
	        	
	        	return div_container;
	        }
        }

        
        exports.init = function (options) {

        	var interdiff_throbber = $("<span/>", {"id": "interdiff_throbber"});
        	interdiff_throbber.text("Loading...");
        	$(options.el).append(interdiff_throbber)

        	interdiff_throbber.show();

            $.getJSON(getPullRequestUrlBase() + '/activities', function(json) {

            	var array_of_deferreds = [];

            	var rescope_reports_by_id = {};
            	
            	$.each(json.values, function(index_activity, activity_object) {
            		if (activity_object.action == "RESCOPED") {

            			var rescope_report = new RescopeReport(activity_object);
            			rescope_reports_by_id[rescope_report.getId()] = rescope_report;

            			if (isTruncatedList(activity_object, "added") || isTruncatedList(activity_object, "removed")) {
            				
            				rescope_report.failure_reason = "Changeset array in rescope data was truncated.";
            				
            			} else {

            				var old_base_hash, new_base_hash;
            				var oldest_added_changeset = getAncestorCommit(activity_object, "added");
            				if (oldest_added_changeset == null) {

            					// XXX Is this right? It's just a guess.
            					new_base_hash = activity_object.fromHash;

            				} else {
            					new_base_hash = oldest_added_changeset.parents[0].id;
            				}

            				var oldest_removed_changeset = getAncestorCommit(activity_object, "removed");
            				if (oldest_removed_changeset == null) {

            					// XXX Is this right? It's just a guess.
            					old_base_hash = activity_object.previousFromHash;

            				} else {
            					old_base_hash = oldest_removed_changeset.parents[0].id;
            				}


            				var deferred = $.ajax({
            					url: getSystemCommandRestUrlBase() + '/api',
            					dataType: "html",
            					data:
                                                {
            					    project_key: getProjectKey(),
            					    repo_slug: getRepoSlug(),
            					    old_base: old_base_hash,
            					    old_tip: activity_object.previousFromHash,
            					    new_base: new_base_hash,
            					    new_tip: activity_object.fromHash
                                                },
            					success: function(data) {
            					    rescope_report.diff_output = data;
            					    rescope_report.success = true;
            					}
                                        });

            				array_of_deferreds.push(deferred);
            			}
            		}
            	});
            	
            	$.when.apply($, array_of_deferreds).done(function() {

            		interdiff_throbber.hide();

        			// Get a list of the dictionary values
        			var sorted_rescope_reports = [];
                	$.each(rescope_reports_by_id, function(report_id, rescope_report) {
                		sorted_rescope_reports.push(rescope_report);
                	});
                	
                	// Sort by date (descending)
        			sorted_rescope_reports.sort(function(b, a) {
        				var a_date = a.getDate();
        				var b_date = b.getDate();
        				return a_date < b_date ? -1 : a_date > b_date ? 1 : 0;
        			});

        			if (sorted_rescope_reports.length > 0) {
        				
            			// Generate table of contents
            			var toc = generateTOC(sorted_rescope_reports);
                		$(options.el).append(toc);
            			
                    	$.each(sorted_rescope_reports, function(index, rescope_report) {
                    		$(options.el).append(rescope_report.getHtmlElement());
                    	});
        				
        			} else {

            			var p = $("<p/>");
            			p.text("No commits have been added or removed since this Pull Request was opened.");
                		$(options.el).append(p);
        			}
        		});
    		});
        };

        exports.reset = function () {
 
        };
    });
