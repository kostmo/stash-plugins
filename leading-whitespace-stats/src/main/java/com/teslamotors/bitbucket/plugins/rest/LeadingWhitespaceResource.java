package com.teslamotors.bitbucket.plugins.rest;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.teslamotors.bitbucket.plugins.GitUtils;
import com.teslamotors.bitbucket.plugins.rest.model.LeadingWhitespaceCounter;
import com.teslamotors.bitbucket.plugins.rest.model.LeadingWhitespaceStatsModel;

/**
 * To query this REST API, use this curl command:
 * curl http://localhost:7990/bitbucket/rest/leading-whitespace/latest/api --user admin:admin
 * 
 * @author kostmo
 *
 */
@Path("/api")
@Produces({MediaType.APPLICATION_JSON})
public class LeadingWhitespaceResource {

    private static final Logger log = LoggerFactory.getLogger(LeadingWhitespaceResource.class);

    private final static String PLUGIN_KEY = "com.teslamotors.bitbucket.plugins.leading-whitespace-stats";
    private final static String WEB_ITEM_KEY = "leading-whitespace-item";
	
	private final ApplicationPropertiesService application_properties_service;
	private final RepositoryService repo_service;

	LeadingWhitespaceResource(ApplicationPropertiesService applicationPropertiesService, RepositoryService repo_service) {
		this.application_properties_service = applicationPropertiesService;
		this.repo_service = repo_service;
	}

	private final static String getModuleKey() {
		return PLUGIN_KEY + ":" + WEB_ITEM_KEY;
	}

	@GET
	public Response getSettings(
			@QueryParam("project_key") final String project_key,
			@QueryParam("repo_slug") final String repo_slug,
			@QueryParam("file_ref") final String file_ref,
			@QueryParam("file_path") final String file_path) throws IOException {

		Repository repository = this.repo_service.getBySlug(project_key, repo_slug);
		File repo_dir = application_properties_service.getRepositoryDir(repository);
		org.eclipse.jgit.lib.Repository git_repo = new FileRepositoryBuilder().setGitDir(repo_dir).build();
		
		InputStream is = GitUtils.getPathInputStreamAtRevision(git_repo, file_ref, new File(file_path));
		LeadingWhitespaceStatsModel model = LeadingWhitespaceCounter.check(is);
		is.close();
		git_repo.close();

		return Response.ok(model).build();
	}
}
