# Line Ending Stats

Displays counts of line endings (LF vs. CRLF) as a "Client Web Item".

# Smoke Test Plan

Click on a source file and verify that the Line ending button shows what type of line endings the file has.

1. Push a file with linux line endings only (/n).
2. Push a file with windows line endings only (/r/n). "awk 'sub("$", "\r")' linux.txt > windows.txt"
3. Push a file with mixed line endings. "head -2 linux.txt > mixed.txt & tail -2 linux.txt | awk 'sub("$", "\r")' >> mixed.txt"